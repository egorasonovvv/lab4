using System;
using Xunit;
using myClass;
using System.Collections.Generic;

namespace myClass.Tests
{
    public class myClassTests
    {
        [Fact]
        public void OneStudentHighestAverageScore()
        {
            List<Student> table = new List<Student>{
                new Student("Jimin", 1, "Math", 3),
                new Student("Jimin", 1, "English", 4),
                new Student("Jimin", 1, "History", 5),
                new Student("Jungkook", 1, "Math", 4),
                new Student("Jungkook", 1, "English", 4),
                new Student("Jungkook", 1, "History", 3),
                new Student("J-Hope", 2, "Math", 5),
                new Student("J-Hope", 2, "English", 5),
                new Student("J-Hope", 2, "History", 5),
                new Student("Ching-Chong", 2, "Math", 3),
                new Student("Ching-Chong", 2, "English", 2),
                new Student("Ching-Chong", 2, "History", 3)
            };

            Assert.Equal(new string[] {"J-Hope"}, StudentsStats.HighestAverageScoreStudents(table));
        }

        [Fact]
        public void TwoStudentHighestAverageScore()
        {
            List<Student> table = new List<Student>{
                new Student("Jimin", 1, "Math", 3),
                new Student("Jimin", 1, "English", 4),
                new Student("Jimin", 1, "History", 5),
                new Student("Jungkook", 1, "Math", 4),
                new Student("Jungkook", 1, "English", 4),
                new Student("Jungkook", 1, "History", 3),
                new Student("J-Hope", 2, "Math", 5),
                new Student("J-Hope", 2, "English", 5),
                new Student("J-Hope", 2, "History", 5),
                new Student("Ching-Chong", 2, "Math", 5),
                new Student("Ching-Chong", 2, "English", 5),
                new Student("Ching-Chong", 2, "History", 5)
            };

            Assert.Equal(new string[] {"Ching-Chong", "J-Hope"}, StudentsStats.HighestAverageScoreStudents(table));
        }

        [Fact]
        public void EachExamAverageScore(){
            List<Student> table = new List<Student>{
                new Student("Jimin", 1, "Math", 3),
                new Student("Jimin", 1, "English", 4),
                new Student("Jimin", 1, "History", 5),
                new Student("Jungkook", 1, "Math", 4),
                new Student("Jungkook", 1, "English", 4),
                new Student("Jungkook", 1, "History", 4),
                new Student("J-Hope", 2, "Math", 5),
                new Student("J-Hope", 2, "English", 5),
                new Student("J-Hope", 2, "History", 5),
                new Student("Ching-Chong", 2, "Math", 5),
                new Student("Ching-Chong", 2, "English", 5),
                new Student("Ching-Chong", 2, "History", 5)
            };

            Assert.Equal(new Dictionary<string, double>() {{"English", 4.5f}, {"History", 4.75f}, {"Math", 4.25f}}, StudentsStats.CountEachExamAverageScore(table));
        }

        [Fact]

        public void EachExamHighestScoreGroup(){
            List<Student> table = new List<Student>{
                new Student("Jimin", 1, "Math", 3),
                new Student("Jimin", 1, "English", 4),
                new Student("Jimin", 1, "History", 5),
                new Student("Jungkook", 1, "Math", 4),
                new Student("Jungkook", 1, "English", 4),
                new Student("Jungkook", 1, "History", 4),
                new Student("J-Hope", 2, "Math", 5),
                new Student("J-Hope", 2, "English", 5),
                new Student("J-Hope", 2, "History", 5),
                new Student("Ching-Chong", 2, "Math", 5),
                new Student("Ching-Chong", 2, "English", 5),
                new Student("Ching-Chong", 2, "History", 5)
            };

            Dictionary<string, List<int>> records = new Dictionary<string, List<int>>() {{"English", new List<int>() {2}}, {"History", new List<int>() {2}}, {"Math", new List<int>() {2}}};

            Assert.Equal(records, StudentsStats.CountEachExamHighestScoreGroup(table));
        }

        [Fact]
        public void EachExamHighestScoreGroups(){
            List<Student> table = new List<Student>{
                new Student("Jimin", 1, "Math", 5),
                new Student("Jimin", 1, "English", 5),
                new Student("Jimin", 1, "History", 5),
                new Student("Jungkook", 1, "Math", 5),
                new Student("Jungkook", 1, "English", 5),
                new Student("Jungkook", 1, "History", 5),
                new Student("J-Hope", 2, "Math", 5),
                new Student("J-Hope", 2, "English", 5),
                new Student("J-Hope", 2, "History", 5),
                new Student("Ching-Chong", 2, "Math", 5),
                new Student("Ching-Chong", 2, "English", 5),
                new Student("Ching-Chong", 2, "History", 5)
            };

            Dictionary<string, List<int>> records = new Dictionary<string, List<int>>() {{"English", new List<int>() {1, 2}}, {"History", new List<int>() {1, 2}}, {"Math", new List<int>() {1, 2}}};

            Assert.Equal(records, StudentsStats.CountEachExamHighestScoreGroup(table));
        }
    }
}
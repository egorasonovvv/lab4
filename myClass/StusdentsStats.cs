﻿using System;
using System.Dynamic;
using System.Collections.Generic;
using System.Linq;

namespace myClass
{
    public record Student (string name, int group, string examName, double score);
    public class StudentsStats
    {
        public static IEnumerable<string> HighestAverageScoreStudents (List<Student> table){
            var res = table.GroupBy(
                student => student.name,
                student => student.score,
                (studentName, studentScores) => new {
                    Key = studentName,
                    AverageScore = Math.Round(studentScores.Average(), 2)
                }).GroupBy(
                    student => student.AverageScore,
                    student => student.Key,
                    (AverageScore, Names) => new{
                        Key = AverageScore,
                        NameArr = Names
                    }
                ).OrderByDescending (student => student.Key).First().NameArr.OrderBy(student => student);

            return res;
        }

        public static Dictionary<string, double> CountEachExamAverageScore (List<Student> table){
            var res = table.GroupBy(
                exam => exam.examName,
                exam => exam.score,
                (examName, examScores) => new {
                    Key = examName,
                    AverageScore = Math.Round(examScores.Average(), 2)
                }).OrderBy (exam => exam.Key).ToDictionary(exam => exam.Key, exam => exam.AverageScore);
                
            return res;
        }

        public static Dictionary<string, List<int>> CountEachExamHighestScoreGroup (List<Student> table){
            var res = table.GroupBy(
                exam => new {examName = exam.examName, groupNumber = exam.group},
                exam => exam.score,
                (pair, examScores) => new {
                    Key = new {examName = pair.examName, groupNumber = pair.groupNumber},
                    AverageScore = Math.Round(examScores.Average(), 2)
                }).GroupBy(
                    exam => new {score = exam.AverageScore, examName = exam.Key.examName},
                    exam => exam.Key.groupNumber,
                    (pair, group) => new {
                        Key = pair.examName,
                        Pair = new {group, pair.score}
                    }
                ).GroupBy(
                    exam => exam.Key,
                    exam => exam.Pair,
                    (examName, pair) => new {
                        Key = examName,
                        HighestScoreGroups = pair.OrderByDescending(exam => exam.score).First().group
                    }
                ).ToDictionary(exam => exam.Key, exam => exam.HighestScoreGroups.ToList<int>());
            
            return res;
        }
    }
}
